import React, { Component } from 'react';
import { View, Text, Image, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import {images} from '../Components/dummyData';

export default class myPrescriptions extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{marginTop: 20}}>
        <FlatList
          data={images}
          renderItem={({item}) => <View style={{flexDirection: 'row',}}>
          <Image source = {item.image} style={{height: 100, width: 100, marginHorizontal: 20, marginVertical: 20,}}/>
          </View>}
          //keyExtractor={(item,index) => index.toString()}
          numColumns={3}
        />
    <View style={{alignItems: 'center'}}>
      <TouchableOpacity onPress={()=>{}} style={styles.appButtonContainer}>
         <Text style={styles.appButtonText}>Done</Text>
      </TouchableOpacity>
    </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#1e90ff",
    borderRadius: 50,
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginTop: 250,
    width: '90%',
    marginLeft: 10,
    marginRight: 10,
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  },
});
