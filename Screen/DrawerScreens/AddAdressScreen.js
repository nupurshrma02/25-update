import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Button, TouchableOpacity, Alert } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import {Appbar, TextInput} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
const AddAddress = ({ navigation }) => {
  
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [Pincode, setPincode] = useState('');
  const [city, setCity] = useState('');
  const [Address, setAddress] = useState('');
  const [Landmark, setLandmark] = useState('');
  const [mobile, setMobile] = useState('');

//    useEffect( async() =>{
//     try {
//         let user = await AsyncStorage.getItem('userdetails');
//         user = JSON.parse(user);
//         setUserid(user.id)
//         //console.log('user id=>', user.id)
//         //console.log('userData =>', user);
//       } catch (error) {
//         console.log(error);
//       }
//   });


  const myfun = async() => {
    let user = await AsyncStorage.getItem('userdetails');
    user = JSON.parse(user);
    //let user_id= setUserid(user.id);
    console.log('user id=>', user.id)
    //Alert.alert(petname);
    //let user_id = {setUserid}
    let dataFetchUrl = 'http://3.12.158.241/medical/api/storeAddress/'+user.id;
    console.log('Addressurl---->', dataFetchUrl);
    await fetch(dataFetchUrl,{
        method : 'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "pincode": Pincode,
          "city_state": city,
          "address": Address,
          "landmark": Landmark,
          "first_name":firstname,
          "last_name" :lastname,
          "mobile": mobile,
                      
     })
    }).then(res => res.json())
    .then(resData => {
       console.log(resData);
       //Alert.alert(resData.message);
       if(resData.msg === 'User Details Created'){
        navigation.navigate('deliveryAddressStack')
       }
    });
 }
  

return(
  <ScrollView style={styles.contentBody}>
  
  <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
      <TextInput
          label="Pincode"
          value={Pincode}
          onChangeText={Pincode => setPincode(Pincode)}
          style={[styles.textInputall, styles.widtthirty]}
          />
      <TextInput
          label="city/state"
          value={city}
          onChangeText={city => setCity(city)}
          style={[styles.textInputall, styles.widtsuxtysix]}
          />
  </View>
  <Text/>
  <Text/>
  <TextInput
      label="First Name"
      value={firstname}
      onChangeText={firstname => setFirstname(firstname)}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      label="Last Name"
      value={lastname}
      onChangeText={lastname => setLastname(lastname)}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      label="Address"
      value={Address}
      onChangeText={Address => setAddress(Address)}
      multiline = {true}
      numberOfLines = {5}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      label="Landmark"
      value={Landmark}
      onChangeText={Landmark => setLandmark(Landmark)}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      label="Mobile No."
      value={mobile}
      onChangeText={mobile => setMobile(mobile)}
      style={styles.textInputall}
      />
  
  <Text/>
  <View style={{alignItems: 'center'}}>
  <TouchableOpacity onPress={myfun} style={styles.appButtonContainer}>
         <Text style={styles.appButtonText}>Add Address</Text>
      </TouchableOpacity>
  </View>
  <Text/>
  </ScrollView>
);
}
export default AddAddress;

const styles = StyleSheet.create({
  image: {
      width: '100%'
  },
  contentBody: {
      paddingHorizontal: 15,
      paddingTop:10,
  },
  textInputall: {
      backgroundColor: '#fff'
  },
  widtthirty: {
      width: '30%',
  },
  widtfortyet: {
      width: '48%',
  },
  radios: {
      flexDirection: 'row',
      justifyContent: 'space-evenly'
  },
  widtsuxtysix: {
      width: '66%',
  },
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#1e90ff",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginTop: 60,
    width: '50%',
    marginLeft: 10,
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  },
});